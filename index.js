// TODO

const express = require('express');
const app = express();
const port = process.env.PORT? process.env.PORT : 56201;

app.use(express.text());

//1. Square
app.post('/square', (req, res) => {
    const number =  parseFloat(req.body);
    if (isNaN(number)) {
        res.status(400).send({ error: 'Invalid or missing number' });
        return;
    }
    const square = number * number;
    res.send({
        number: number,
        square: square,
    });
});


//2.Reverse
app.post('/reverse', (req, res) => {
    const text = req.body;
    const reversed = text.split('').reverse().join('');
    res.send(reversed);
});


//3.Date
app.get('/date/:year/:month/:day', (req, res) => {
    const year =  parseInt(req.params.year);
    const month =  parseInt(req.params.month);
    const day =  parseInt(req.params.day);
    if (isNaN(year) || year < 1000 || year > 9999 || isNaN(month) || month < 1 || month > 12 || isNaN(day) || day < 1 || day > 31)
    {
        res.status(400).send({ error: 'Invalid or missing parameters for a date' });
        return;
    }
    const date = new Date(year,month-1,day, 0, 0, 0, 0);
    if(day !== date.getDate() || month-1 !== date.getMonth() || year !== date.getFullYear()) {
        console.log(date.toISOString().split('T')[0]);
        res.status(400).send({error: 'Invalid date'});
        return;
    }
    const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekDay = weekDays[date.getDay()];
    const isLeapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    const difference = Math.abs(Math.floor((Date.now() - date) / (1000 * 60 * 60 * 24)));
    res.send({
        weekDay: weekDay,
        isLeapYear: isLeapYear,
        difference: difference,
    })
});


/*//Square request
fetch(`http://127.0.0.1:${port}/square`, {
    method: 'POST',
    headers: {
        'Content-Type': 'text/plain'
    },
    body: '7'
})
    .then(response => response.json())
    .then(number => console.log(number))
    .then(square => console.log(square))
    .catch(error => console.error('Error:', error));

//Reverse request
fetch(`http://127.0.0.1:${port}/reverse`, {
    method: 'POST',
    headers: {
        'Content-Type': 'text/plain'
    },
    body: 'ChipiChipiChapaChapa'
})
    .then(response => response.text())
    .then(reversedText => console.log(reversedText))
    .catch(error => console.error('Error:', error));

//Date data
const year = 2024;
const month = 2;
const day = 29;

//Date request
fetch(`http://127.0.0.1:56201/date/${year}/${month}/${day}`, {
    method: 'GET',
})
    .then(response => response.json())
    .then(weekDay => console.log(weekDay))
    .then(isLeapYear => console.log(isLeapYear))
    .then(difference => console.log(difference))
    .catch(error => console.error('Error:', error));*/

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
});